//    Lords of Time Client, a client for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate rmp_serde;

extern crate rustls;
extern crate webpki_roots;

use rmp_serde::decode;

use std::io;
use std::io::{Read,Write};
use std::net::TcpStream;
use std::thread;
use std::env;

mod verbs;
use verbs::{ClientVerb,ServerVerb};
fn main() {
    let addr = env::args().skip(1).next().expect("No address provided");
    let mut stream = TcpStream::connect(addr).unwrap();
    let stream_clone = stream.try_clone().unwrap();
    thread::spawn(move || listen(stream_clone));
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();
        stream.write_all(rmp_serde::to_vec(&ClientVerb::Broadcast(input.trim().to_owned())).unwrap().as_slice()).expect("Failed to write to server");
    }
}
fn listen(stream: TcpStream) {
    let mut de = decode::Deserializer::new(stream);
    loop {
        match serde::Deserialize::deserialize(&mut de) {
            Ok(verb) => {
                match verb {
                    ServerVerb::Broadcast(message,user) => {
                        println!("{}: {}",user,message);
                    },
                    _ => {}
                }
            },
            Err(e) => {
                panic!("Server error: {}",e);
            }
        }
    }
    
}

