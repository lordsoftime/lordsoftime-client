//    Lords of Time Client, a client for the virtual card game Lords of Time
//    Copyright (C) 2017  Waylon Cude <waylon531@firechicken.net>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

type Index = u32;
type Username = String;
type Password = String;
#[derive(Serialize,Deserialize,Debug)]
pub struct GameId {
    id: u64
}
//All u32s will be converted to usize for indexing
#[derive(Deserialize,Serialize,Debug)]
pub struct Targets {
    self_targets: Vec<Index>,
    opposing_targets: Vec<Index>
}
#[derive(Deserialize,Serialize,Debug)]
pub enum Action {
    PlayCard(Index,Targets), //Index into hand

    ActivateCard(Index,Index,Targets),  //Activate ability on monster
                                        //Index to monster, index to ability, target

    Attack(Vec<Index>), //Attack with selected monsters

    PassPriority, //Is it called priority?
    EndTurn //Ends turn, no further actions can be taken
}
#[derive(Deserialize)]
pub enum Error {
    PermissionDenied,
    AuthError(AuthError),
    DeserializationError,
}
#[derive(Deserialize,Debug,PartialEq)]
pub enum AuthError {
    BadPassword,
    //This error shouldn't happen if there are no bugs
    InvalidUsername,
    BadInput,
    MultipleUsers,
    DBError,
    EnvError,
    IOError,
    UserNotFound,
    UsernameReserved
}
#[derive(Serialize,Debug)]
pub enum ClientVerb {
    Auth(Username,Password),    //User,Pass

    Broadcast(String),      //Broadcast message to all users

    Register(Username,Password),//User, Pass 
                            //Should email be required for registration?
                            //It's probably a good idea for password reset

    Challenge(Username),      //Challenge player with given username
                            //If other player challenges you a game is created
                            //and each player is notified

    Listen(GameId),         //Returns game state
                            //and sends subsequent updates to client

    GetState(GameId),       //Returns game state but not subsequent updates

    DoAction(GameId,Action),//Do an action in the specified game
}
#[derive(Deserialize)]
pub enum ServerVerb {
    Result(Result<(),Error>),       //Returned on every command not explicitly

    Broadcast(String,Username),              //Sent to all connected players 
                                    // on broadcast

    Challenge(Username),            //User got a challenge from username
                                    
    Action(GameId,Action),          //Sent to all listening players
                                    //in a game

    //State(Result<(GameId,game::SendState),Error>)   //Returned on GetState and Listen
}
